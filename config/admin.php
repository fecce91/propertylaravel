<?php

return [
	/*
	 * Admin title
	 * Displays in page title and header
	 */
	'title'                 => 'Администритивная панель',

	/*
	 * Admin url prefix
	 */
	'prefix'                => 'area51',

	/*
	 * Before filters to protect admin from unauthorized users
	 */
	'beforeFilters'         => ['admin.auth'],

	/*
	 * Path to admin bootstrap files directory in app directory
	 * Default: 'app/admin'
	 */
	'bootstrapDirectory'    => app_path('admin'),

	/*
	 * Path to images directory
	 * Default: 'public/images'
	 */
	'imagesDirectory'       => public_path('uploads/images'),

	/*
	 * Path to files directory
 	 * Default: 'public/files'
 	 */
	'filesDirectory'        => public_path('uploads/files'),

	/*
	 * Path to images upload directory within 'imagesDirectory'
	 * Default: 'uploads'
	 */
	'imagesUploadDirectory' => '',

	/*
	 * Authentication config
	 */
	'auth'                  => [
		'model'  => \SleepingOwl\AdminAuth\Entities\Administrator::class,
		'rules' => [
			'username' => 'required',
			'password' => 'required',
		]
	],

    /*
	 * Blade template prefix, default admin::
	 */
    'bladePrefix'                => 'admin::',

	'template' => 'SleepingOwl\Admin\Templates\TemplateDefault'
];
