process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');

var paths = {
    'jquery': './vendor/bower_components/jquery/dist/',
    'bootstrap': './vendor/bower_components/bootstrap/dist/',
    'fotorama': './vendor/bower_components/fotorama/'
}

elixir(function (mix) {
    mix
        //copy all vendor scripts, stylesheets to resource directory
        .copy(paths.jquery + 'jquery.min.js', 'resources/assets/js/jquery')
        .copy(paths.bootstrap + 'js/bootstrap.min.js', 'resources/assets/js/bootstrap/bootstrap.min.js')
        .copy(paths.fotorama + 'fotorama.css', 'resources/assets/less/fotorama/fotorama.less')
        .copy(paths.fotorama + 'fotorama.js', 'resources/assets/js/fotorama/fotorama.js')
        //copy fonts and other static materials directly to public
        .copy(paths.bootstrap + 'fonts', 'public/fonts')
        .copy(paths.fotorama + 'fotorama.png', 'public/build/css/')
        .copy(paths.fotorama + 'fotorama@2x.png', 'public/build/css/')
        //precompile app and vendor less files
        .less([
            'import.less',
            'fotorama/fotorama.less',
            'layout.less'
        ])
        //vendor and app js files
        .scripts([
            'jquery/jquery.min.js',
            'bootstrap/bootstrap.min.js',
            'fotorama/fotorama.js',
            'common.js',
            'retina.min.js'
        ])
        //adding version control to css and js files
        .version([
            'js/all.js',
            'css/app.css'
        ]);
});
