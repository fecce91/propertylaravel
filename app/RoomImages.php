<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Intervention\Image\Facades\Image;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class RoomImages
 * @package App
 *
 * @property Room|null $room
 * @property int|null $room_id
 * @property \Intervention\Image\Image $image
 * @property string $created_at
 * @property string $updated_at
 */
class RoomImages extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    protected $guarded = array(
        'id'
    );

    public function getImageFields()
    {
        return array(
            'image' => 'room_images/'
        );
    }

    public function setImage($field, $image)
    {
        parent::setImage($field, $image);
        $file = $this->$field;
        if (!$file->exists()) return;
        $path = $file->getFullPath();

        Image::make($path)
            ->fit(1000, 550)
            ->save()
            ->fit(1000 * 2, 550 * 2)
            ->save(preg_replace('/(\.gif|\.jpg|\.jpeg|\.png)/', '@2x$1', $path));
    }

    /**
     * @return BelongsTo
     */
    public function room() {
        return $this->belongsTo(Room::class);
    }
}
