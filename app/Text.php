<?php

namespace App;

use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class Text
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 */
class Text extends SleepingOwlModel
{
    protected $guarded = array(
        'id'
    );
}
