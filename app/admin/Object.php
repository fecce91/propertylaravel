<?php

Admin::model(App\Object::class)
    ->title('Объекты')
    ->columns(function () {
        Column::image('image', 'Изображение');
        Column::string('name', 'Наименование');

        Column::string('short_description', 'Краткое описание');

        Column::count('images', 'Фото')
            ->append(Column::filter('object_id')->model(App\ObjectImages::class));
        Column::count('rooms', 'Помещений')
            ->append(Column::filter('object_id')->model(App\Room::class));

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $name = FormItem::text('name', 'Наименование');
        $name->required();
        $name->validationRule('string|max:50');

        $space = FormItem::text('space', 'Площадь');
        $space->validationRule('integer');

        $of = FormItem::text('offices', 'Офисов');
        $of->validationRule('integer');

        $sd = FormItem::textarea('short_description', 'Краткое содержимое');
        $sd->validationRule('string|max:125');

        $md = FormItem::textarea('medium_description', 'Среднее содержимое');
        $md->validationRule('string');

        $ld = FormItem::ckeditor('long_description', 'Полное содержимое');
        $ld->validationRule('string');

        $geo_x = FormItem::text('geo_x', 'Гео координаты X');
        $geo_x->validationRule('string|max:10');

        $geo_y = FormItem::text('geo_y', 'Гео координаты Y');
        $geo_y->validationRule('string|max:10');

        $image = FormItem::image('image', 'Изображение');
        $image->validationRule('image');
    });