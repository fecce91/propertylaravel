<?php

use SleepingOwl\Admin\Admin;

Admin::menu()
    ->url('/')
    ->label('Статус')
    ->icon('fa-dashboard')
    ->uses('\App\Http\Controllers\PageController@adminDashboard');

Admin::menu(\App\Text::class)
    ->url('texts')
    ->label('Тексты')
    ->icon('fa-header');

Admin::menu(\App\Feedback::class)
    ->url('feedbacks')
    ->label('Обратная связь')
    ->icon('fa-question-circle');

Admin::menu(\App\Service::class)
    ->url('services')
    ->label('Услуги')
    ->icon('fa-list');

Admin::menu(\App\News::class)
    ->url('news')
    ->label('Новости')
    ->icon('fa-file-text-o');

Admin::menu()
    ->label('Управление объектами')
    ->icon('fa-list-alt')
    ->items(function () {
        Admin::menu(\App\Object::class)
            ->url('objects')
            ->label('Объекты')
            ->icon('fa-building-o');

        Admin::menu(\App\ObjectImages::class)
            ->url('object_images')
            ->label('Изображения')
            ->icon('fa-file-image-o');

        Admin::menu()
            ->label('Управление помещениями')
            ->icon('fa-list-alt')
            ->items(function() {
                Admin::menu(\App\Room::class)
                    ->url('rooms')
                    ->label('Помещения')
                    ->icon('fa-square-o');

                Admin::menu(\App\RoomImages::class)
                    ->url('room_images')
                    ->label('Изображения')
                    ->icon('fa-file-image-o');
            });
    });