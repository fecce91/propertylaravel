<?php

Admin::model(App\News::class)
    ->title('Новости')
    //->denyEditing()
    ->columns(function () {
        Column::image('image', 'Изображение');
        Column::string('name', 'Наименование');

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
        $up->sortableDefault();
    })
    ->form(function () {
        $name = FormItem::text('name', 'Наименование');
        $name->required();
        $name->validationRule('string|max:50');

        $cat = FormItem::textarea('cat', 'Краткое содержимое');
        $cat->validationRule('string');

        $content = FormItem::ckeditor('content', 'Полное содержимое');
        $content->validationRule('string');

        $image = FormItem::image('image', 'Изображение');
        $image->required(true);
        $image->validationRule('image');
    });