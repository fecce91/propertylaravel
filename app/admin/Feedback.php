<?php

Admin::model(App\Feedback::class)
    ->title('Обратная связь')
    ->denyCreating()
    ->denyDeleting()
    ->filters(function() {
        ModelItem::filter('object_id')->title()->from(\App\Object::class, 'name');
    })
    ->columns(function () {
        Column::string('object.name', 'Объект');

        Column::string('name', 'Имя');
        Column::string('email', 'Email');
        Column::string('phone', 'Телефон');

        $cp = Column::date('created_at', 'Создано');
        $cp->format('short', 'short');
        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $object = FormItem::select('object_id', 'Объект');
        $object->nullable();
        $object->list(\App\Object::class);

        $name = FormItem::text('name', 'Имя');
        $name->validationRule('string|max:50');

        $email = FormItem::text('email', 'Email');
        $email->validationRule('email|max:125');

        $phone = FormItem::text('phone', 'Телефон');
        $phone->validationRule('digits:11');

        $comments = FormItem::textarea('comments', 'Заметка о заявке (заполняется сотрудником компании)');
        $comments->validationRule('string');
    });