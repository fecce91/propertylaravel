<?php

Admin::model(App\ObjectImages::class)
    ->title('Изображения объектов')
    ->filters(function() {
        ModelItem::filter('object_id')->title()->from(\App\Object::class, 'name');
    })
    ->columns(function () {
        Column::image('image', 'Изображение');

        Column::string('object.name', 'Объект');

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $object = FormItem::select('object_id', 'Объект');
        $object->list(\App\Object::class);
        $object->required();

        $image = FormItem::image('image', 'Изображение');
        $image->required(true);
        $image->validationRule('image');
    });