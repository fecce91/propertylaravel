<?php

Admin::model(App\Room::class)
    ->title('Помещения объектов')
    ->filters(function() {
        ModelItem::filter('object_id')->title()->from(\App\Object::class, 'name');
    })
    ->columns(function () {
        Column::string('object.name', 'Объект');

        Column::string('floor', 'Этаж');
        Column::string('space', 'Площадь');
        Column::string('available', 'Доступность');

        Column::count('images', 'Фото')
            ->append(Column::filter('room_id')->model(App\RoomImages::class));

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $object = FormItem::select('object_id', 'Объект');
        $object->list(\App\Object::class);
        $object->required();

        $floor = FormItem::text('floor', 'Этаж');
        $floor->validationRule('integer');

        $space = FormItem::text('space', 'Площадь');
        $space->validationRule('numeric');

        FormItem::checkbox('available', 'Доступен');
    });