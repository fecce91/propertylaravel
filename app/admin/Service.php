<?php

Admin::model(App\Service::class)
    ->title('Услуги')
    ->columns(function () {
        Column::image('image', 'Изображение');

        Column::string('name', 'Наименование');

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $name = FormItem::text('name', 'Наименование');
        $name->required();
        $name->validationRule('string|max:50');

        $description = FormItem::textarea('description', 'Краткое описание');
        $description->validationRule('string');

        $content = FormItem::ckeditor('content', 'Содержание');
        $content->validationRule('string');

        $image = FormItem::image('image', 'Изображение');
        $image->required(true);
        $image->validationRule('image');
    });