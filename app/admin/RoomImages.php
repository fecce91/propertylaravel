<?php

Admin::model(App\RoomImages::class)
    ->title('Изображения помещений')
    ->filters(function() {
        ModelItem::filter('room_id')->title()->from(\App\Room::class, 'name');
    })
    ->columns(function () {
        Column::image('image', 'Изображение');

        Column::string('room.object.name', 'Объект');
        Column::string('room.name', 'Помещение');

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () {
        $object = FormItem::select('room_id', 'Помещение');
        $object->list(\App\Room::class);
        $object->required();

        $image = FormItem::image('image', 'Изображение');
        $image->required(true);
        $image->validationRule('image');
    });