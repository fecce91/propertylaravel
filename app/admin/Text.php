<?php

$admin = Admin::model(App\Text::class);


$admin
    ->title('Тексты')
    ->denyDeleting()
    ->denyCreating()
    ->columns(function () {
        Column::string('name', 'Наименование');

        $up = Column::date('updated_at', 'Обновлено');
        $up->format('short', 'short');
    })
    ->form(function () use ($admin) {
        $admin->title('Редактирование текста');

        $description = FormItem::textarea('text', 'Текст');
        /* @var $description \SleepingOwl\Admin\Admin */
        $description->validationRule('string');
    });