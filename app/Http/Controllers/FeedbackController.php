<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Valid;
use Illuminate\Http\Request;
use Mail;

class FeedbackController extends Controller
{
    public function create(Request $request)
    {
        //strange situation, I must check one field
        //for 2 type of validation rules: email and phone number
        //if it`s a phone, need to fill email field in db
        //if it`s a email, need to fill phone field in db
        //if it`s a something else, than show error
        $credentials = trim($request->get('credentials'));

        $feedback = new Feedback();

        if (Valid::email($credentials))
            $feedback->email = $request->get('credentials');
        else {
            $firstLetter = substr($credentials, 0, 1);
            if ($firstLetter === '7' || $firstLetter === '8') {
                $credentials = '+7' . substr($credentials, 1);
            } elseif (mb_strlen($credentials) === 10) {
                $credentials = '+7' . $credentials;
            }

            if (Valid::phone($credentials, 'RU')) {
                $feedback->phone = substr($credentials, 1);
            } else {
                return redirect()->back()->with('message', [
                    'title' => 'Не верно введены реквизиты',
                    'text' => 'Вы неверно ввели реквизиты для обратной связи.<br />Пожалуйста введите ваш email или номер телефона и мы с вами обязательно свяжемся.'
                ]);
            }
        }

        $object_id = $request->get('object_id', false);
        if (Valid::integer($object_id))
            $feedback->object_id = $object_id;
        $name = $request->get('name', false);
        if (is_string($name))
            $feedback->name = $name;

        $feedback->save();

        if (env('MAIL_SEND_TO')) {
            Mail::send('email.feedback_notification', ['feedback' => $feedback], function ($m) use ($feedback) {
                $m->to(env('MAIL_SEND_TO'))->from(env('MAIL_USERNAME'))->subject('На сайт propertym.ru пришла заявка');
            });
        }

        return redirect()->back()->with('message', [
            'title' => 'Ваша заявка принята',
            'text' => 'Спасибо! Мы свяжемся с вами в ближайшее время.'
        ]);
    }
}
