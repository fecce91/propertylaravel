<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\News;
use App\Text;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function showAll()
    {
        $news = News::orderBy('weight', 'asc')->get();

        $header = Text::find(1);

        return view('page/news-all', array(
            'news' => $news,
            'header' => $header
        ));
    }

    public function showByID(Request $request, $id)
    {
        $news = News::where('id', $id)->first();

        return redirect(route('news:name', array('name' => $news->name)), 301);
    }

    public function showByName(Request $request, $name)
    {
        $news = News::where('name', $name)->first();

        $prev = News::withoutOrders()->where('weight', '<', $news->weight)->orderBy('weight', 'desc')->first();
        $next = News::withoutOrders()->where('weight', '>', $news->weight)->orderBy('weight', 'asc')->first();

        return view('page/news', array(
            'news' => $news,
            'prev' => $prev,
            'next' => $next
        ));
    }
}
