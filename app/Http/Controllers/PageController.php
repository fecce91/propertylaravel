<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\News;
use App\Object;
use App\Service;
use App\Text;
use Route;
use View;

class PageController extends Controller
{

    public function main()
    {
        $objects = Object::orderBy('weight')->get();

        $header_1 = Text::find(3);
        $header_2 = Text::find(4);

        return view('page/main', array(
            'objects' => $objects,
            'header_1' => $header_1,
            'header_2' => $header_2
        ));
    }

    public function contacts()
    {
        $text = Text::find(5);

        return view('page/contacts', array(
            'text' => $text
        ));
    }

    public function service()
    {
        $services = \App\Service::orderBy('weight', 'asc')->get();

        $header = Text::find(2);

        return view('page/service', array(
            'services' => $services,
            'header' => $header
        ));
    }

    public function adminDashboard() {

        return View::make('dashboard', array(
            'count' => array(
                'objects' => Object::count(),
                'feedback' => Feedback::count(),
                'services' => Service::count(),
                'news' => News::count(),
            )
        ));
    }
}
