<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Http\Requests,
    App\Object;

class ObjectController extends Controller
{
    public function showAll()
    {
        $objects = Object::orderBy('weight')->get();

        return view('page/object-all', array(
            'objects' => $objects
        ));
    }

    public function showByID(Request $request, $id)
    {
        $object = Object::find($id);
        if(!$object)
            abort(404, 'Объект с ID ' . $id . ' не найден');

        return view('page/object', array(
            'object' => $object
        ));
    }

    public function showByName(Request $request, $name)
    {
        $object = Object::where('name', $name)->first();
        if(!$object)
            abort(404, 'Объект с Названием ' . $name . ' не найден');

        return view('page/object', array(
            'object' => $object
        ));
    }
}
