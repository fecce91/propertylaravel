<?php

//main page
Route::get('/', array(
    'as' => 'main',
    'uses' => 'PageController@main'
));

//contacts page
Route::get('/contacts', array(
    'uses' => 'PageController@contacts'
));

//service page
Route::get('/service', array(
    'uses' => 'PageController@service'
));

//feedback REST methods
Route::group(array(
    'as' => 'feedback:'
), function() {
    Route::put('/feedback', array(
        'as' => 'put',
        'uses' => 'FeedbackController@create'
    ));
});

Route::group(array(
    'as' => 'news:'
), function() {
    Route::get('/news', array(
        'as' => 'all',
        'uses' => 'NewsController@showAll'
    ));

    //news by id
    Route::get('/news/{id}', array(
        'as' => 'id',
        'uses' => 'NewsController@showByID'
    ))
        ->where('id', '[0-9]+');

    //news by name
    Route::get('/news/{name}', array(
        'as' => 'name',
        'uses' => 'NewsController@showByName'
    ))
        ->where('name', '.+');
});

//objects page group
Route::group(array(
    'as' => 'object:'
), function () {
    //all objects
    Route::get('/object', array(
        'as' => 'all',
        'uses' => 'ObjectController@showAll'
    ));

    //object by id
    Route::get('/object/{id}', array(
        'as' => 'id',
        'uses' => 'ObjectController@showByID'
    ))
        ->where('id', '[0-9]+');

    //object by name
    Route::get('/object/{name}', array(
        'as' => 'name',
        'uses' => 'ObjectController@showByName'
    ))
        ->where('name', '.+');
});


/*
 * Application static menu
 */


Menu::make('example', function ($menu) {
    $menu->add('О нас', '');
    $menu->add('Услуги', 'service');

    $menu
        ->add('Объекты', array('route' => 'object:all'))
        ->active('object/*');

    $menu
        ->add('Новости', 'news')
        ->active('news/*');

    $menu->add('Контакты', 'contacts');
});




