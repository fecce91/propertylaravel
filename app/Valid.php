<?php

namespace app;

use Validator;

/**
 * Class Valid
 * @package app
 *
 * @method static bool phone(string $string, string $countryCode)
 * @method static bool email(string $string)
 * @method static bool integer(string $string)
 */
class Valid
{
    private function __construct() {}

    public static function __callStatic($name, array $arguments) {
        if(!isset($arguments[0]))
            return false;

        if(isset($arguments[1])) {
            $name .= ':' . $arguments[1];
        }

        $validator = Validator::make(array(
            'value' => $arguments[0]
        ), array(
            'value' => $name
        ));

        return $validator->passes();
    }
}