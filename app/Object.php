<?php

namespace App;

use Intervention\Image\Facades\Image;
use MyProject\Proxies\__CG__\stdClass;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Interfaces\ModelWithOrderFieldInterface;
use SleepingOwl\Models\Traits\ModelWithOrderFieldTrait;
use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class Object
 * @property string $geo_x
 * @property string $geo_y
 * @property string $name
 */
class Object extends SleepingOwlModel implements ModelWithImageFieldsInterface, ModelWithOrderFieldInterface
{
    use ModelWithOrderFieldTrait;

    protected $guarded = array(
        'id'
    );

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function images() {
        return $this->hasMany(ObjectImages::class);
    }

    public function getImageFields()
    {
        return array(
            'image' => 'objects/'
        );
    }

    public function setImage($field, $image)
    {
        parent::setImage($field, $image);
        $file = $this->$field;
        if (!$file->exists()) return;
        $path = $file->getFullPath();

        // вы можете использовать возможности пакета Intervention Image для изменения изображения
        Image::make($path)
            ->fit(1024, 1024)
            ->save()
            ->fit(1024 * 2, 1024 * 2)
            ->save(preg_replace('/(\.gif|\.jpg|\.jpeg|\.png)/', '@2x$1', $path));
    }

    public static function getList() {
        $objects = static::all();

        $result = array();

        foreach($objects as $object) {
            $result[$object->id] = $object->name;
        }

        return $result;
    }

    public function getSortField()
    {
        return 'weight';
    }

    /**
     * @return stdClass
     */
    public function getGeoInfo() {
        $geo = new \stdClass();

        $geo->x = $this->geo_x;
        $geo->y = $this->geo_y;
        $geo->name = 'БЦ "' . $this->name . '"';

        return $geo;
    }
}
