<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Intervention\Image\Image;
use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class Room
 * @package App
 *
 * @property string $name
 * @property Object $object
 * @property RoomImages[] $images
 * @property int $object_id
 * @property int $floor
 * @property int $space
 * @property int $available
 * @property string $created_at
 * @property string $updated_at
 */
class Room extends SleepingOwlModel
{
    protected $guarded = array(
        'id'
    );

    /**
     * @return BelongsTo
     */
    public function object()
    {
        return $this->belongsTo(Object::class);
    }

    /**
     * @return HasMany
     */
    public function images() {
        return $this->hasMany(RoomImages::class);
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $rooms = new self();
        $rooms = $rooms
            ->with('object')
            ->orderBy('object_id', 'ASC')
            ->orderBy('name', 'ASC')
            ->get();

        $result = array();

        foreach ($rooms as $room) {
            $result[$room->id] = '(' . $room->object->name . ') ' . $room->name;
        }

        return $result;
    }
}
