<?php

namespace App;

use Intervention\Image\Facades\Image;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Interfaces\ModelWithOrderFieldInterface;
use SleepingOwl\Models\Traits\ModelWithOrderFieldTrait;
use SleepingOwl\Models\SleepingOwlModel;

class Service extends SleepingOwlModel implements ModelWithImageFieldsInterface, ModelWithOrderFieldInterface
{
    use ModelWithOrderFieldTrait;

    protected $guarded = array(
        'id'
    );

    public function getImageFields()
    {
        return array(
            'image' => 'service/'
        );
    }

    public function setImage($field, $image)
    {
        parent::setImage($field, $image);
        $file = $this->$field;
        if (!$file->exists())
            return;
        $path = $file->getFullPath();

        Image::make($path)
            ->fit(420, 280)
            ->save()
            ->fit(420 * 2, 280 * 2)
            ->save(preg_replace('/(\.gif|\.jpg|\.jpeg|\.png)/', '@2x$1', $path));
    }

    public function getSortField()
    {
        return 'weight';
    }
}
