<?php

namespace App;

use Intervention\Image\Facades\Image;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Interfaces\ModelWithOrderFieldInterface;
use SleepingOwl\Models\Traits\ModelWithOrderFieldTrait;

class News extends SleepingOwlModel implements ModelWithImageFieldsInterface, ModelWithOrderFieldInterface
{
    use ModelWithOrderFieldTrait;

    protected $guarded = array(
        'id'
    );

    public function getImageFields()
    {
        return array(
            'image' => 'news/'
        );
    }

    public function setImage($field, $image)
    {
        parent::setImage($field, $image);

        $file = $this->$field;
        /* @var $image \Intervention\Image\Image */

        if (!$file->exists())
            return;

        $path = $file->getFullPath();

        // вы можете использовать возможности пакета Intervention Image для изменения изображения
        $image = Image::make($path)
            ->fit(1027, 683)
            ->save()
            ->fit(1027 * 2, 683 * 2)
            ->save(preg_replace('/(\.gif|\.jpg|\.jpeg|\.png)/', '@2x$1', $path));
    }

    public function getSortField()
    {
        return 'weight';
    }
}
