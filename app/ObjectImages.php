<?php

namespace App;

use Intervention\Image\Facades\Image;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class ObjectImages
 * @package App
 *
 * @property Object|null $object
 * @property int|null $object_id
 * @property \Intervention\Image\Image $image
 * @property string $created_at
 * @property string $updated_at
 */
class ObjectImages extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    protected $guarded = array(
        'id'
    );

    public function getImageFields()
    {
        return array(
            'image' => 'object_images/'
        );
    }

    public function setImage($field, $image)
    {
        parent::setImage($field, $image);
        $file = $this->$field;
        if (!$file->exists())
            return;
        $path = $file->getFullPath();

        Image::make($path)
            ->fit(1000, 550)
            ->save()
            ->fit(1000 * 2, 550 * 2)
            ->save(preg_replace('/(\.gif|\.jpg|\.jpeg|\.png)/', '@2x$1', $path));
    }

    public function object()
    {
        return $this->belongsTo(Object::class);
    }
}
