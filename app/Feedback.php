<?php

namespace App;

use SleepingOwl\Models\SleepingOwlModel;

/**
 * Class Feedback
 * @package App
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property int|null $phone
 * @property string|null $comments
 * @property Object $object
 * @property int|null $object_id
 */
class Feedback extends SleepingOwlModel
{
    protected $guarded = array(
        'id'
    );

    public function object()
    {
        return $this->belongsTo(Object::class);
    }
}
