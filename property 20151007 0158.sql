﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.358.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 07.10.2015 1:58:36
-- Версия сервера: 5.6.26-log
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Описание для таблицы administrators
--
DROP TABLE IF EXISTS administrators;
CREATE TABLE administrators (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(190) NOT NULL,
  password VARCHAR(60) NOT NULL,
  name VARCHAR(255) NOT NULL,
  remember_token VARCHAR(100) DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (id),
  UNIQUE INDEX administrators_username_unique (username)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы migrations
--
DROP TABLE IF EXISTS migrations;
CREATE TABLE migrations (
  migration VARCHAR(255) NOT NULL,
  batch INT(11) NOT NULL
)
ENGINE = INNODB
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы news
--
DROP TABLE IF EXISTS news;
CREATE TABLE news (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  cat TEXT DEFAULT NULL,
  content TEXT DEFAULT NULL,
  image TEXT DEFAULT NULL,
  weight INT(10) DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  imageConfirmDelete VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_news_name (name)
)
ENGINE = INNODB
AUTO_INCREMENT = 8
AVG_ROW_LENGTH = 2730
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы objects
--
DROP TABLE IF EXISTS objects;
CREATE TABLE objects (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  short_description VARCHAR(125) DEFAULT NULL,
  medium_description TEXT DEFAULT NULL,
  long_description TEXT DEFAULT NULL,
  weight INT(10) UNSIGNED DEFAULT NULL,
  space INT(10) UNSIGNED DEFAULT NULL,
  offices INT(10) UNSIGNED DEFAULT NULL,
  image VARCHAR(255) DEFAULT NULL,
  imageConfirmDelete VARCHAR(10) DEFAULT NULL,
  geo_x VARCHAR(10) DEFAULT NULL,
  geo_y VARCHAR(10) DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы password_resets
--
DROP TABLE IF EXISTS password_resets;
CREATE TABLE password_resets (
  email VARCHAR(255) NOT NULL,
  token VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  INDEX password_resets_email_index (email),
  INDEX password_resets_token_index (token)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы services
--
DROP TABLE IF EXISTS services;
CREATE TABLE services (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  description TEXT DEFAULT NULL,
  content TEXT NOT NULL,
  image VARCHAR(255) NOT NULL,
  weight INT(10) UNSIGNED DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  imageConfirmDelete VARCHAR(10) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы texts
--
DROP TABLE IF EXISTS texts;
CREATE TABLE texts (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  text TEXT DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы users
--
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(60) NOT NULL,
  remember_token VARCHAR(100) DEFAULT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (id),
  UNIQUE INDEX users_email_unique (email)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы feedbacks
--
DROP TABLE IF EXISTS feedbacks;
CREATE TABLE feedbacks (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL,
  email VARCHAR(125) DEFAULT NULL,
  phone BIGINT(11) DEFAULT NULL,
  comments TEXT DEFAULT NULL,
  object_id INT(11) UNSIGNED DEFAULT NULL,
  created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT FK_feedbacks_objects_id FOREIGN KEY (object_id)
    REFERENCES objects(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 35
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы object_images
--
DROP TABLE IF EXISTS object_images;
CREATE TABLE object_images (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  object_id INT(10) UNSIGNED DEFAULT NULL,
  image VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT FK_object_images_objects_id FOREIGN KEY (object_id)
    REFERENCES objects(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 37
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы rooms
--
DROP TABLE IF EXISTS rooms;
CREATE TABLE rooms (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  object_id INT(10) UNSIGNED NOT NULL,
  floor INT(10) DEFAULT NULL,
  space DECIMAL(10, 2) DEFAULT NULL,
  available TINYINT(1) NOT NULL DEFAULT 0,
  name VARCHAR(50) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT FK_rooms_objects_id FOREIGN KEY (object_id)
    REFERENCES objects(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 8
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

--
-- Описание для таблицы room_images
--
DROP TABLE IF EXISTS room_images;
CREATE TABLE room_images (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  room_id INT(10) UNSIGNED DEFAULT NULL,
  image VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  CONSTRAINT FK_room_images_rooms_id FOREIGN KEY (room_id)
    REFERENCES rooms(id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 41
AVG_ROW_LENGTH = 655
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы administrators
--
INSERT INTO administrators VALUES
(1, 'admin', '$2y$10$jTeUdnoQJgc41uqgKSQ7wOsseYAF1qjaWO9h8IqI2kDa0aK2X8PNu', 'Samoylov Eugene', NULL, '2015-09-03 12:16:48', '2015-09-07 10:24:42');

-- 
-- Вывод данных для таблицы migrations
--
INSERT INTO migrations VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_10_12_104748_create_administrators_table', 2);

-- 
-- Вывод данных для таблицы news
--
INSERT INTO news VALUES
(1, 'Мы посетили конференцию года', 'ачимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение систем массового участия. Идейные соображения высшего порядка, а также новая модель организационн', '<p>222Значимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение систем массового участия. Идейные соображения высшего порядка, а также новая модель организационной деятельности позволяет оценить значение новых предложений. Повседневная практика показывает, что консультация с широким активом обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации.</p>\r\n\r\n<p>Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки соответствующий условий активизации. Задача организации, в особенности же сложившаяся структура организации способствует подготовки и реализации дальнейших направлений развития. С другой стороны дальнейшее развитие различных форм деятельности способствует подготовки и реализации соответствующий условий активизации. Разнообразный и богатый опыт укрепление и развитие структуры требуют от нас анализа систем массового участия. Идейные соображения высшего порядка, а также начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке систем массового участия.</p>\r\n\r\n<p>Равным образом рамки и место обучения кадров в значительной степени обуславливает создание направлений прогрессивного развития. Не следует, однако забывать, что начало повседневной работы по формированию позиции играет важную роль в формировании систем массового участия. Задача организации, в особенности же дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий.</p>\r\n', 'CnnTUN9c7T.jpeg', 1, '2015-07-21 18:42:02', '2015-10-06 21:04:05', NULL),
(2, 'День рождения Генерального директора', 'Сегодня день рождение у нашего Генерального директора', '<p>111Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца. Я совсем один и блаженствую в здешнем краю, словно созданном для таких, как я. Я так счастлив, мой друг, так упоен ощущением покоя, что искусство мое страдает от этого. Ни одного штриха не мог бы я сделать, а никогда не был таким большим художником, как в эти минуты. Когда от милой моей долины поднимается пар и полдневное солнце стоит над непроницаемой чащей темного леса и лишь редкий луч проскальзывает в его святая святых, а я лежу в высокой траве у быстрого ручья и, прильнув</p>\r\n', '2tG5Ih7gm0.jpeg', 0, '2015-07-21 18:43:31', '2015-10-06 20:47:00', NULL),
(3, 'Тест 1', 'В этом году Property Managment учавствовал в конференции', '<p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца. Я совсем один и блаженствую в здешнем краю, словно созданном для таких, как я. Я так счастлив, мой друг, так упоен ощущением покоя, что искусство мое страдает от этого. Ни одного штриха не мог бы я сделать, а никогда не был таким большим художником, как в эти минуты. Когда от милой моей долины поднимается пар и полдневное солнце стоит над непроницаемой чащей темного леса и лишь редкий луч проскальзывает в его святая святых, а я лежу в высокой траве у быстрого ручья и, прильнув</p>\r\n\r\n<p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить</p>\r\n\r\n<p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая строчка рыбного текста по имени Lorem ipsum решила выйти в большой мир грамматики. Великий Оксмокс предупреждал ее о злых запятых, диких знаках вопроса и коварных точках с запятой, но текст не дал сбить себя с толку. Он собрал семь своих заглавных букв, подпоясал инициал за пояс и пустился в дорогу. Взобравшись на первую вершину курсивных гор, бросил он последний взгляд назад, на силуэт своего родного города Буквоград, на заголовок деревни Алфавит и на подзаголовок своего переулка Строчка. Грустный реторический вопрос скатился по его щеке и он продолжил свой путь. По дороге встретил текст рукопись. Она предупредила его: &laquo;В моей стране все переписывается по несколько раз. Единственное, что от меня осталось, это приставка &laquo;и&raquo;. Возвращайся ты лучше в свою безопасную страну&raquo;. Не послушавшись рукописи, наш текст продолжил свой путь. Вскоре ему повстречался коварный составитель рекламных текстов, напоивший его языком и речью и заманивший в свое агенство, которое использовало его снова и снова в своих проектах. И если его не переписали, то живет он там до сих пор.Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Вдали от всех живут они в буквенных домах на берегу Семантика большого языкового океана. Маленький ручеек Даль журчит по всей стране и обеспечивает ее всеми необходимыми правилами. Эта парадигматическая страна, в которой жаренные члены предложения залетают прямо в рот. Даже всемогущая пунктуация не имеет власти над рыбными текстами, ведущими безорфографичный образ жизни. Однажды одна маленькая</p>\r\n', '9mk9P02j0F.png', 2, '2015-07-21 18:42:02', '2015-09-03 12:06:27', NULL),
(4, 'Тест 2', 'Сегодня день рождение у нашего Генерального директора', '<p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца. Я совсем один и блаженствую в здешнем краю, словно созданном для таких, как я. Я так счастлив, мой друг, так упоен ощущением покоя, что искусство мое страдает от этого. Ни одного штриха не мог бы я сделать, а никогда не был таким большим художником, как в эти минуты. Когда от милой моей долины поднимается пар и полдневное солнце стоит над непроницаемой чащей темного леса и лишь редкий луч проскальзывает в его святая святых, а я лежу в высокой траве у быстрого ручья и, прильнув</p>\r\n', 'BL6It48e4H.jpeg', 3, '2015-07-21 18:43:31', '2015-10-06 20:30:36', 'on'),
(5, 'Тест 3', 'Сегодня день рождение у нашего Генерального директора', '<p>Душа моя озарена неземной радостью, как эти чудесные весенние утра, которыми я наслаждаюсь от всего сердца. Я совсем один и блаженствую в здешнем краю, словно созданном для таких, как я. Я так счастлив, мой друг, так упоен ощущением покоя, что искусство мое страдает от этого. Ни одного штриха не мог бы я сделать, а никогда не был таким большим художником, как в эти минуты. Когда от милой моей долины поднимается пар и полдневное солнце стоит над непроницаемой чащей темного леса и лишь редкий луч проскальзывает в его святая святых, а я лежу в высокой траве у быстрого ручья и, прильнув</p>\r\n', 'KJJ9xsMnNE.jpeg', 4, '2015-07-21 18:43:31', '2015-10-06 20:30:05', 'on'),
(7, 'Final check', 'This is a final check cat ', '<blockquote>\r\n<p><strong>This is a final check content</strong></p>\r\n</blockquote>\r\n', 'BXBhZbLLKQ.jpeg', 5, '2015-09-03 12:07:22', '2015-09-03 12:08:15', NULL);

-- 
-- Вывод данных для таблицы objects
--
INSERT INTO objects VALUES
(1, 'Neo Geo', 'Новый бизнес-центр с полной инфраструктурой и торговыми площадями', 'Инфраструктура «Neo Geo» будет представлена фитнес-центром, удобным как для сотрудников, так и для гостей комплекса, кафе, ресторанами, и отделениями банков. В бизнес-центре располагаются магазины и объекты городской инфраструктуры. В бизнес-центре предус', '<p>Инфраструктура &laquo;Neo Geo&raquo; будет представлена фитнес-центром, удобным как для сотрудников, так и для гостей комплекса, кафе, ресторанами, и отделениями банков. В бизнес-центре располагаются&nbsp;магазины и объекты городской инфраструктуры. В бизнес-центре предусмотрены просторные подземный и наземный&nbsp;парковки.</p>\r\n\r\n<p>Свободные площади в &laquo;Neo Geo&raquo; предлагаются компанией Property Management как в продажу, так и в аренду.&nbsp;Наличие больших и малых офисных, а также торговых блоков в бизнес-центре позволит с комфортом разместиться&nbsp;как крупному бизнесу, так и молодой динамично развивающейся компании. В данном бизнес-центре в аренду также предлагаются представительские этажи с панорамными видами в башне комплекса.</p>\r\n', 3, 150000, 23, 'A9FruN3I3y.jpeg', 'on', '55.65015', '37.540587', '2015-07-16 17:45:08', '2015-09-27 21:47:01'),
(2, 'Гранд Сетунь Плаза', 'Великолепный бизнес-центр с эффективными планировк', 'Инфраструктура бизнес-центра «Гранд Сетунь Плаза» представлена ресторанами, кафе и столовой, современным фитнес-центром, а также удобным для сотрудников магазинами. На территории бизнес центра предусмотрена парковка для сотрудников и гостей комплекса.    ', '<p>Инфраструктура бизнес-центра &laquo;Гранд Сетунь Плаза&raquo; представлена ресторанами, кафе и столовой, современным фитнес-центром, а также удобным для сотрудников магазинами. На территории бизнес центра предусмотрена парковка для сотрудников и гостей комплекса. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n\r\n<p>Бизнес-центр был введен в эксплуатацию в 2012 году, и уже сейчас он заполнен арендаторами на 70%. Property managment предлагает своим партнерам аренду и покупку свободных офисных площадей, как больших, так и с возможным &nbsp;делением. В данном бизнес-центре доступна покупка арендного бизнеса.&nbsp;</p>\r\n', 1, 1234000, 153, 'OD47o2qY6z.jpeg', NULL, '55.725924', '37.396901', '2015-07-16 17:45:55', '2015-09-27 21:45:25'),
(3, 'Ярд', 'Английский стиль в 5 минутах от центра Москвы', 'Служба охраны бизнес-центра работает круглосуточно, что позволяет арендаторам всегда чувствовать себя в безопасности, обеспеченной системой видеонаблюдения и сотрудниками ЧОПа. «Магистраль Плаза» открыт для сотрудников круглосуточно, 7 дней в неделю. Охра', '<p>Служба охраны бизнес-центра работает круглосуточно, что позволяет арендаторам всегда чувствовать себя в безопасности, обеспеченной системой видеонаблюдения и сотрудниками ЧОПа.&nbsp;&laquo;Магистраль Плаза&raquo; открыт&nbsp;для сотрудников круглосуточно, 7 дней в неделю. Охраняемые паркинги также функционируют без перерывов.</p>\r\n\r\n<p>Введенный в эксплуатацию в 2009 году, сегодня бизнес-центр &laquo;Магистраль Плаза&raquo; заполнен арендаторами&nbsp;на&nbsp;95%. Property Management предлагает своим партнерам свободные офисные площади в бизнес-центре&nbsp;на&nbsp;выгодных&nbsp;условиях.</p>\r\n', 0, 30000, 58, '7vZLkYc3En.jpeg', NULL, '55.766283', '37.526851', '2015-07-16 18:01:06', '2015-10-06 20:33:56'),
(4, 'Магистраль Плаза', '5 минут до самого центра Москвы на автомобиле', 'Служба охраны бизнес-центра работает круглосуточно, что позволяет арендаторам всегда чувствовать себя в безопасности, обеспеченной системой видеонаблюдения и сотрудниками ЧОПа. «Магистраль Плаза» открыт для сотрудников круглосуточно, 7 дней в неделю. Охра', '<p>Служба охраны бизнес-центра работает круглосуточно, что позволяет арендаторам всегда чувствовать себя в безопасности, обеспеченной системой видеонаблюдения и сотрудниками ЧОПа.&nbsp;&laquo;Магистраль Плаза&raquo; открыт&nbsp;для сотрудников круглосуточно, 7 дней в неделю. Охраняемые паркинги также функционируют без перерывов.</p>\r\n\r\n<p>Введенный в эксплуатацию в 2009 году, сегодня бизнес-центр &laquo;Магистраль Плаза&raquo; заполнен арендаторами&nbsp;на&nbsp;95%. Property Management предлагает своим партнерам свободные офисные площади в бизнес-центре&nbsp;на&nbsp;выгодных&nbsp;условиях.</p>\r\n', 2, 575000, 68, 'Fg4vyODYJz.jpeg', NULL, '55.766653', '37.529232', '2015-07-16 18:01:25', '2015-09-27 21:46:12');

-- 
-- Вывод данных для таблицы password_resets
--

-- Таблица property.password_resets не содержит данных

-- 
-- Вывод данных для таблицы services
--
INSERT INTO services VALUES
(1, 'Полное управление помещением Заказчика', 'И вот этим это удобно\r\nИ вот этим это удобно', '<p>Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, мТекст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений.</p>\r\n', 'X8aetd27he.jpeg', 0, '2015-09-09 09:22:34', '2015-10-05 21:56:55', NULL),
(2, 'Консультационная работа с Заказчиком', 'И вот так вот и вот так вот\r\nИ вот так вот и вот так вот вот вот вот', '<p>Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, мТекст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений, Текст про ремонт помещений.</p>\r\n', '8JVE6TrqF5.jpeg', 1, '2015-09-09 09:23:19', '2015-10-05 21:56:55', NULL);

-- 
-- Вывод данных для таблицы texts
--
INSERT INTO texts VALUES
(1, 'Новости', '111Мы очень много работаем, чтобы предоставлять нашим клиентам лучший сервис на рынке.\r\nВ этом разделе мы расскажем вам о наших событиях и новостях.', '2015-10-07 01:23:47', '2015-10-06 22:40:02'),
(2, 'Услуги', 'Мы работаем с 2010 года и оказываем самые лучшие услуги, которые многие и не думаю оказывать.\r\nМы делаем все, чтобы вы могли сосредоточиться на главном - на вашем бизнесе.', '2015-10-07 01:43:43', '2015-10-06 22:44:23'),
(3, 'Главная Строка 1', 'ЛУЧШИЕ УСЛОВИЯ ДЛЯ ВАШЕГО БИЗНЕСА', '2015-10-07 01:46:26', '2015-10-06 22:46:53'),
(4, 'Главная Строка 2', 'Компания Property Management предоставляет услуги управления офисной недвижимостью. Мы - молодая компания, созданная опытными и энергичными профессионалами рынка. Мы работаем с наиболее знаковыми для города бизнес-центрами класса В+: Ярд, Гранд Сетунь Плаза, Магистраль Плаза и Neo Geo. В нашем управлении находится более  20.000 кв.м. Основные цели нашей работы – успех бизнеса инвестора и комфорт арендатора.', '2015-10-07 01:46:35', '2015-10-06 22:50:17'),
(5, 'Контакты', '        <div class="row">\r\n            <p class="text-center .text-normal col-lg-12">\r\n                Вы всегда можете позвонить нам по номеру <strong>+7 (495) 974 99 99</strong>\r\n            </p>\r\n        </div>\r\n        <div class="row">\r\n            <p class="text-center .text-normal col-lg-12">Или вы можете написать на почту\r\n                <strong>\r\n                    <a href="mailto::hello@propertym.ru">hello@propertym.ru</a>\r\n                </strong>\r\n            </p>\r\n        </div>\r\n        <div class="row">\r\n            <p class="text-center .text-normal col-lg-12">Или вы можете приехать к нам в офис по адресу: <strong>г. Москва,\r\n                    Магистральный тупик, д. 5</strong></p>\r\n        </div>\r\n        <div class="row map-row">\r\n            <div class="map" id="map-draw" data-height="600px"\r\n                 data-center-x="55.766299"\r\n                 data-center-y="37.529403"\r\n                 data-zoom="16"\r\n                 data-geo="[{&quot;x&quot;:&quot;55.766299&quot;,&quot;y&quot;:&quot;37.529403&quot;,&quot;name&quot;:&quot;\\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, \\u041c\\u0430\\u0433\\u0438\\u0441\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0442\\u0443\\u043f\\u0438\\u043a, \\u0434.5&quot;}]\r\n"></div>\r\n        </div>', '2015-10-07 01:50:45', '2015-10-06 22:54:16');

-- 
-- Вывод данных для таблицы users
--

-- Таблица property.users не содержит данных

-- 
-- Вывод данных для таблицы feedbacks
--
INSERT INTO feedbacks VALUES
(26, NULL, NULL, 79260692903, NULL, NULL, '2015-10-05 20:19:32', '2015-10-05 20:19:32'),
(27, NULL, NULL, 79260692902, NULL, NULL, '2015-10-05 20:19:36', '2015-10-05 20:19:36'),
(28, NULL, NULL, 79260692902, NULL, NULL, '2015-10-05 20:19:42', '2015-10-05 20:19:42'),
(30, NULL, NULL, 79260692902, NULL, NULL, '2015-10-05 20:21:45', '2015-10-05 20:21:45'),
(31, NULL, NULL, 79260692902, NULL, NULL, '2015-10-05 20:22:14', '2015-10-05 20:22:14'),
(32, NULL, NULL, 79260692902, NULL, NULL, '2015-10-05 20:22:21', '2015-10-05 20:22:21'),
(33, NULL, 'samoylov@nexters.com', NULL, NULL, NULL, '2015-10-05 20:22:29', '2015-10-05 20:22:29'),
(34, 'Евгений Самойлов', NULL, 79260692902, NULL, 2, '2015-10-05 20:28:45', '2015-10-05 20:28:45');

-- 
-- Вывод данных для таблицы object_images
--

-- Таблица property.object_images не содержит данных

-- 
-- Вывод данных для таблицы rooms
--
INSERT INTO rooms VALUES
(5, 1, 5, 100.13, 1, 'На 5м этаже', '2015-09-08 15:06:10', '2015-10-05 22:03:54'),
(6, 1, 2, 100.00, 1, 'На 2м этаже', '2015-09-08 15:06:27', '2015-10-05 20:45:47'),
(7, 1, 1, 1111.00, 0, 'На 1м этаже', '2015-09-08 15:06:37', '2015-10-05 20:45:53');

-- 
-- Вывод данных для таблицы room_images
--
INSERT INTO room_images VALUES
(36, 7, 'bAhQrneads.jpeg', '2015-10-05 21:26:50', '2015-10-05 21:26:50'),
(37, 6, 'vgo0kinhNo.jpeg', '2015-10-05 21:26:58', '2015-10-05 21:26:58'),
(38, 5, '9nfp3akKIi.jpeg', '2015-10-05 21:27:03', '2015-10-05 21:27:03'),
(39, 5, '8K5Ga1kXTv.jpeg', '2015-10-05 21:31:22', '2015-10-05 21:31:22'),
(40, 5, 'JEztGH3Kk0.jpeg', '2015-10-05 21:31:28', '2015-10-05 21:31:28');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;