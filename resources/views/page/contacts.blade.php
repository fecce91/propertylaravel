@extends('layout')

@section('page-class', 'contacts')
@section('title', 'Контакты')
@section('content-container-class', 'container-fluid')

@section('content')
    <section>
        @if (isset($text))
            {!! $text->text !!}
        @endif

        <div class="row map-row">
            <div class="map" id="map-draw" data-height="600px"
                 data-center-x="55.766299"
                 data-center-y="37.529403"
                 data-zoom="11"
                 data-geo="[{&quot;x&quot;:&quot;55.766299&quot;,&quot;y&quot;:&quot;37.529403&quot;,&quot;name&quot;:&quot;\u041c\u043e\u0441\u043a\u0432\u0430, \u041c\u0430\u0433\u0438\u0441\u0442\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0442\u0443\u043f\u0438\u043a, \u0434.5&quot;}]"></div>
        </div>
    </section>
@endsection