@extends('layout')

@section('page-class', 'news-all')
@section('title', 'Новости')
@section('body-color-class', 'light')

@if (isset($header))
    @section('text-welcome', $header->text)
@endif

<?php
function template_news_create_article($article, $i, $disk)
{
?>

<article class="article col-lg-5 col-md-5 col-sm-6 col-xs-12
    @if($i % 2 === 0) col-lg-offset-1 col-md-offset-1 @endif">
    <div class="row">
        <div class="col-lg-12 article-image">
            <a href="{{ route('news:name', array('name' => $article->name))  }}">
                @if ($article->image->exists())
                    @if ($disk->exists('images/news/' . $article->image))
                        <img src="/uploads/images/news/{{ $article->image }}">
                    @endif
                @endif
            </a>
        </div>

        <div class="col-lg-12 article-cat">
            <a href="{{ route('news:name', array('name' => $article->name))  }}">
                {{ $article->cat }}
            </a>
        </div>
    </div>
</article>

<?php
}
?>

@section('content')
    <section>
        <?php
        $disk = Storage::disk('local');
        $count = count($news);
        ?>

        @for ($i = 0; $i < $count; $i++)
            <div class="row">
                @if (isset($news[$i]))
                    {!! template_news_create_article($news[$i], $i, $disk) !!}
                @endif

                <?php $i++; ?>

                @if (isset($news[$i]))
                    {!! template_news_create_article($news[$i], $i, $disk) !!}
                @endif
            </div>
        @endfor
    </section>
@endsection