@extends('layout')

@section('page-class', 'object')
@section('title', $object->name)
@section('body-color-class', 'light')

@section('content')
    <section class="object-holder">
        <article class="row object-header">
            <div class="col-lg-6 col-md-6 col-sm-6 object-images fotorama" data-nav="thumbs" data-allowfullscreen="true"
                 data-fit="scaledown" data-thumbfit="cover" data-loop="true" data-autoplay="5000">

                <?php
                $disk = Storage::disk('uploads_images');
                $images = $object->images;
                ?>

                @foreach($images as $image)
                    @if ($disk->exists($image->image->getPath()))
                        <img class="object-image" src="/uploads/images/{{ $image->image->getPath() }}">
                    @endif
                @endforeach
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="row">
                    <h1 class="object-name col-lg-12 object-caption">БЦ &laquo;{{ $object->name }}&raquo;</h1>
                </div>
                <div class="row">
                    <p class="object-medium-description col-lg-12">{{ $object->medium_description }}</p>
                </div>
                <div class="row">
                    <div class="col-lg-12 object-feedback-button-holder">
                        <button type="button" class="btn btn-property btn-danger" data-toggle="modal"
                                data-target="#object-feedback">Заявка на просмотр
                        </button>
                    </div>
                </div>
            </div>
        </article>
        <article class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="object-caption">Описание</h2>

                <div class="object-long-description text-normal">
                    {!! $object->long_description !!}
                </div>
            </div>
        </article>

        <?php
        /* @var $object \App\Object */
        $rooms = $object->rooms()->orderBy('floor')->get();
        /* @var $rooms \App\Room */
        ?>

        @if (count($rooms))
            <article class="row rooms">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="object-caption">Помещения</h2>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @foreach ($rooms as $room)
                        <?php
                        /* @var $room \App\Room */
                        $images = $room->images;
                        ?>

                        <div class="room row">
                            <div class="room-cell room-image col-sm-2 col-xs-12 fotorama fotorama-fullscreen-thumbs" data-allowfullscreen="true"
                                 data-fit="scaledown" data-nav="false" data-loop="true" data-autoplay="5000" data-arrows="false">
                                @foreach($images as $image)
                                    @if ($disk->exists($image->image->getPath()))
                                        <img class="object-image" src="/uploads/images/{{ $image->image->getPath() }}">
                                    @endif
                                @endforeach
                            </div>

                            <div class="room-cell room-space col-sm-2 col-xs-4">
                                @if ($room->space)
                                    <span class="room-cell-caption">Площадь:</span> {{ (float) $room->space, '2' }}м
                                @else
                                    &nbsp;
                                @endif
                            </div>
                            <div class="room-cell room-floor col-sm-2 col-xs-4">
                                @if ($room->floor)
                                    <span class="room-cell-caption">Этаж:</span> {{ $room->floor }}
                                @else
                                    &nbsp;
                                @endif
                            </div>
                            <div class="room-cell room-available col-sm-2 col-xs-4">
                                <span class="room-cell-caption">Статус:</span>
                                @if ($room->available)
                                    <span class="room-available-true">Свободно</span>
                                @else
                                    <span class="room-available-false">Сдано</span>
                                @endif
                            </div>
                            <div class="room-cell room-feedback col-sm-4 col-xs-12">
                                @if ($room->available)
                                    <button type="button" class="btn btn-property btn-danger" data-toggle="modal"
                                            data-target="#object-feedback">
                                        Посмотреть объект
                                    </button>
                                @else
                                    <button type="button" class="btn btn-property btn-danger btn-two-line"
                                            data-toggle="modal" data-target="#object-feedback">
                                        Сообщить,<br/>когда освободится
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </article>
        @endif

        <div class="modal fade" id="object-feedback">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    {!! Form::open(array('route' => 'feedback:put', 'method' => 'put')) !!}

                    {!! Form::hidden('object_id', $object->id) !!}

                    <div class="modal-body">
                        <button type="button" class="close outside" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>

                        <div class="modal-object-feedback">
                            <h4 class="modal-title">Заявка на просмотр</h4>

                            <div class="form-group">
                                {!! Form::text('name', null, array(
                                    'class' => 'form-control',
                                    'id' => 'object-feedback-name',
                                    'placeholder' => 'Ваше имя'
                                )) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::text('credentials', null, array(
                                    'class' => 'form-control',
                                    'id' => 'object-feedback-phone',
                                    'placeholder' => 'Ваш контактный телефон или E-mail'
                                )) !!}
                            </div>

                            {!! Form::button('Свяжитесь со мной', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-property modal-object-submit'
                            )) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection