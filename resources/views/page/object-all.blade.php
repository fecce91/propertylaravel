@extends('layout')

@section('page-class', 'object-all')
@section('title', 'Объекты')
@section('content-container-class', 'container-fluid')
@section('body-color-class', 'light')

@section('content')
    <?php
        /* @var $objects \App\Object[] */
        $objects_count = count($objects);
        $disk = Storage::disk('uploads_images');

        $geo = array();
        foreach($objects as $object) {
            $geo[] = $object->getGeoInfo();
        }

        $geo = json_encode($geo);
    ?>

    <section class="row">
        <div class="col-md-6 map" id="map-draw" data-height-match="#objects-list" data-geo="{{ $geo }}"></div>
        <div class="col-md-6 col-sm-12 objects-list" id="objects-list">
            @foreach ($objects as $object)
                @if ($disk->exists($object->image->getPath()))
                    <article class="object-item row">
                        <a href="{{ route('object:name', array('name' => $object->name))  }}">
                            <div class="col-lg-3 col-md-4 col-sm-6 object-image">
                                <img src="/uploads/images/{{ $object->image->getPath() }}">
                            </div>
                            <div class="col-lg-9 col-md-8 col-sm-6 object-description">
                                <h2 class="object-name">
                                    БЦ &laquo;{{ $object->name }}&raquo;
                                </h2>

                                <p class="object-properties text-normal">
                                    @if ($object->space)
                                        Общая площадь: {{ $object->space }} м<sup>2</sup><br/>
                                    @endif
                                    @if ($object->offices)
                                        Количество офисов: {{ $object->offices }}
                                    @endif
                                </p>
                            </div>
                        </a>
                    </article>
                @endif
            @endforeach
        </div>
    </section>
@endsection