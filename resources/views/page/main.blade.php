@extends('layout')

@section('page-class', 'main')
@section('body-class', 'main')
@section('header-class', 'main')
@section('body-color-class', 'light')
@section('logo-modifier', 'white')

@section('content')
    <div class="objects-list row">
        <?php
        $disk = Storage::disk('uploads_images');
        $objects_count = count($objects);
        ?>

        @for ($i = 0; $i < $objects_count; $i++)
            <a class="object-item col-lg-4 col-md-4 col-sm-6 @if($i % 2 === 1) col-md-offset-0 col-sm-offset-0 @else col-lg-offset-2 col-md-offset-2 @endif"
               href="{{ route('object:name', array('name' => $objects[$i]->name)) }}">
                <div class="object-wrapper">
                    @if ($disk->exists($objects[$i]->image->getPath()))
                        <img class="object-image" src="/uploads/images/{{ $objects[$i]->image->getPath() }}">
                    @endif

                    <div class="object-background"></div>

                    <h2 class="object-name text-center text-uppercase">БЦ {{ $objects[$i]->name }}</h2>

                    <p class="object-description text-center">{{ $objects[$i]->short_description }}</p>
                </div>
            </a>
        @endfor
    </div>
@endsection