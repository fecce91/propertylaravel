@extends('layout')

@section('page-class', 'article')
@section('title', $news->name)
@section('body-color-class', 'light')

@section('content')
    <section class="article">
        @if ($news->image)
            <?php
            $disk = Storage::disk('uploads_images');
            ?>

            @if ($disk->exists($news->image->getPath()))
                <div class="row">
                    <div class="article-image-holder col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12 col-xs-offset-0 text-center">
                        <img src="/uploads/images/news/{{ $news->image }}">
                    </div>
                </div>
            @endif
        @endif
        <div class="row">
            <div class="article-name-holder col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
                <h1 class="article-name text-center text-attention-big text-weight-bold">{{ $news->name }}</h1>
            </div>
        </div>
        <div class="row">
            <div class="article-content-holder col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="article-content">{!! $news->content !!}</div>
            </div>
        </div>

        <div class="row news-paginator">
            <div class="col-xs-6 col-sm-4 previous">
                @if(isset($prev))
                    <a href="{{ route('news:name', array('name' => $prev->name))  }}">Предыдущая новость</a>
                @endif
            </div>
            <div class="col-xs-6 col-sm-4 col-sm-offset-4 next">
                @if(isset($next))
                    <a href="{{ route('news:name', array('name' => $next->name))  }}">Следующая новость</a>
                @endif
            </div>
        </div>
    </section>
@endsection