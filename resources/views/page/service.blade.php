@extends('layout')

@section('page-class', 'service')
@section('title', 'Услуги')
@section('content-container-class', 'container')
@section('body-color-class', 'light')

@if (isset($header))
    @section('text-welcome', $header->text)
@endif

@section('content')
    <?php $disk = Storage::disk('uploads_images'); ?>

    <section>
        <div class="service-list">
            @foreach($services as $service)
                <a href="javascript:void(0);" class="about-button" data-toggle="modal"
                   data-target="#service-modal-{{ $service->id }}">
                    <div class="service row">
                        <div class="service-image table-cell col-lg-3 col-lg-offset-1 col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0">
                            @if ($disk->exists($service->image->getPath()))
                                <img src="/uploads/images/{{ $service->image->getPath() }}">
                            @endif
                        </div>
                        <div class="service-description table-cell col-lg-7 col-md-8 col-sm-8">
                            <h2 class="name">{{ $service->name }}</h2>

                            <p class="description">{!! nl2br($service->description) !!}</p>
                        </div>
                    </div>
                </a>

                <div class="modal fade" id="service-modal-{{ $service->id }}" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <button type="button" class="close outside" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>

                            <div class="modal-body">
                                <h4 class="modal-title">{{ $service->name }}</h4>

                                {!! $service->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection