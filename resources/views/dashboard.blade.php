<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Статус</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-question-circle fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $count['feedback'] }}</div>
                        <div>{!! \App\Helper::numMorph($count['feedback'], array('Вопрос', 'Вопроса', 'Вопросов')) !!} в
                            обратной связи
                        </div>
                    </div>
                </div>
            </div>
            <a href="/area51/feedbacks">
                <div class="panel-footer">
                    <span class="pull-left">Посмотреть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $count['services'] }}</div>
                        <div>{!! \App\Helper::numMorph($count['services'], array('Услуга', 'Услуги', 'Услуг')) !!}</div>
                    </div>
                </div>
            </div>
            <a href="/area51/services">
                <div class="panel-footer">
                    <span class="pull-left">Посмотреть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $count['news'] }}</div>
                        <div>{!! \App\Helper::numMorph($count['news'], array('Новость', 'Новости', 'Новостей')) !!}</div>
                    </div>
                </div>
            </div>
            <a href="/area51/news">
                <div class="panel-footer">
                    <span class="pull-left">Посмотреть</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-building-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $count['objects'] }}</div>
                        <div>{!! \App\Helper::numMorph($count['objects'], array('Объект', 'Объекта', 'Объектов')) !!}</div>
                    </div>
                </div>
            </div>
            <a href="/area51/objects">
                <div class="panel-footer">
                    <span class="pull-left">Объекты</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
            <a href="/area51/object_images">
                <div class="panel-footer">
                    <span class="pull-left">Изображения объектов</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
            <a href="/area51/rooms">
                <div class="panel-footer">
                    <span class="pull-left">Помещения</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
            <a href="/area51/room_images">
                <div class="panel-footer">
                    <span class="pull-left">Изображения помещений</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>