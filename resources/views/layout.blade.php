<!DOCTYPE html>
<html lang="ru">
<head>
    @if (trim($__env->yieldContent('title')))
        <title>@yield('title') | Property Management</title>
    @else
        <title>Property Management</title>
    @endif

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}" type="text/css">
</head>
<body class="page-@yield('body-class', 'default') color-@yield('body-color-class', 'default')">
<div id="body-wrapper">
    @if (Session::has('message'))
        <?php $message = Session::pull('message'); ?>

        <div class="modal fade session-message onload">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close outside" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">{{ $message['title'] or 'Внимание!' }}</h3>
                        <p class="text-center">{!! $message['text'] or '' !!}</p>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div id="header-wrapper" class="@yield('header-class', 'default')">
        <div class="header-wrapper-background">
            <header id="header" class="container @yield('header-class', 'default')">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12 text-center">
                        <a href="/">
                            <img src="/images/logo-@yield('logo-modifier', 'colored').png">
                        </a>
                    </div>

                    <div class="col-md-7 col-md-offset-2 col-sm-12 col-xs-12">
                        <nav id="main-menu" class="menu row">
                            <?php $i = 0; ?>
                            @foreach ($menu_example->all() as $item)
                                <a class="{{ $item->attribute('class') }} text-center col-lg-2 col-md-2 col-sm-2 col-xs-6 @if ($i === 0) col-lg-offset-2 col-md-offset-2 col-sm-offset-1 col-xs-offset-0 @endif"
                                   href="{{ $item->url() }}">
                                    <span>{{ $item->title }}</span>
                                </a>
                                <?php $i++ ?>
                            @endforeach
                        </nav>
                    </div>
                </div>

                <div class="slogan-holder row">
                    <h1 class="slogan text-center text-uppercase col-md-12">
                        @if (isset($header_1))
                            {{ $header_1->text }}
                        @endif
                    </h1>

                    <p class="description col-md-8 col-md-offset-2 text-center">
                        @if (isset($header_2))
                            {{ $header_2->text }}
                        @endif
                    </p>
                </div>
            </header>
        </div>
    </div>

    @if ($__env->yieldContent('text-welcome'))
        <div id="content-welcome-wrapper">
            <div id="content-welcome" class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                        @yield('text-welcome')
                    </div>
                </div>
            </div>
        </div>
    @endif


    <div id="content-wrapper">
        <div id="content" class="@yield('content-container-class', 'container') page-@yield('page-class', 'default')">
            @section('content')
                No Content
            @show
        </div>
    </div>

    <footer id="footer" class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <h2 class="footer-slogan col-lg-12 col-md-12 col-sm-12 col-xs-12 text-uppercase text-center">Нет
                            времени?</h2>
                    </div>

                    <div class="row">
                        <p class="footer-description col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">Оставьте
                            e-mail или телефон и мы подберем помещение для вас</p>
                    </div>

                    <div class="row">
                        {!! Form::open(array('route' => 'feedback:put', 'class' => 'feedback-form', 'method' => 'put')) !!}
                        {!! Form::text('credentials', null, array(
                            'class' => 'credentials col-lg-3 col-lg-offset-3 col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1',
                            'placeholder' => 'Ваш e-mail или телефон'
                        )) !!}
                        <div class="text-center feedback-submit-holder col-lg-3 col-lg-offset-0 col-md-3 col-md-offset-0 col-sm-5 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                            {!! Form::button('Свяжитесь со мной', array(
                                'type' => 'submit',
                                'class' => 'feedback-submit text-uppercase'
                            )) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="row">
                        <div class="copyright col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <small>Property Management &copy; 2015</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="{{ elixir('js/all.js') }}" type="text/javascript"></script>
</body>
</html>
