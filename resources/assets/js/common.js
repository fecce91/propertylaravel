$(window).load(function () {
    //подключаем yandex карту, если есть элемент с ид map-draw
    var map_draw = $('#map-draw');

    $(window).resize(function () {
        yandex_map_draw(map_draw);
    });

    yandex_map_draw(map_draw);
});

$(document).ready(function () {
    $('.fotorama.fotorama-fullscreen-thumbs').on(
        'fotorama:fullscreenenter fotorama:fullscreenexit',
        function (e, fotorama, extra) {
            if (e.type == "fotorama:fullscreenenter")
                fotorama.setOptions({nav: "thumbs"});
            else
                fotorama.setOptions({nav: false});
        });

    $('.modal.onload').modal('show');
});

var ya_created = false;
var map;
function yandex_map_draw(map_draw) {
    if ($(window).width() >= 992 && map_draw.length) {
        if (ya_created === false) {
            //data block height
            if (typeof(map_draw.attr('data-height-match')) !== 'undefined' && map_draw.attr('data-height-match').length) {
                var height_match = $(map_draw.attr('data-height-match'));

                if (height_match.length)
                    map_draw.css('height', height_match.css('height'));
            } else if (typeof(map_draw.attr('data-height')) !== 'undefined' && map_draw.attr('data-height').length)
                map_draw.css('height', map_draw.attr('data-height'));

            //data geo geo array
            if (typeof(map_draw.attr('data-geo')) !== 'undefined' && map_draw.attr('data-geo').length) {
                var geo = $.parseJSON(map_draw.attr('data-geo'));
            }

            //data center x
            if (typeof(map_draw.attr('data-center-x')) !== 'undefined' && map_draw.attr('data-center-x').length)
                var center_x = map_draw.attr('data-center-x');
            else
                var center_x = '55.76';

            //data center x
            if (typeof(map_draw.attr('data-center-y')) !== 'undefined' && map_draw.attr('data-center-y').length)
                var center_y = map_draw.attr('data-center-y');
            else
                var center_y = '37.64';

            //data zoom
            if (typeof(map_draw.attr('data-zoom')) !== 'undefined' && map_draw.attr('data-zoom').length)
                var zoom = map_draw.attr('data-zoom');
            else
                var zoom = 11;

            ya_created = true;

            $.getScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU', function (data, textStatus, jqxhr) {
                    ymaps.ready(function () {
                        map = new ymaps.Map("map-draw", {
                            center: [center_x, center_y],
                            zoom: zoom,
                            controls: ["zoomControl"]
                        });

                        map.behaviors.disable('scrollZoom');

                        if (typeof(geo) === 'object') {
                            $.each(geo, function (index, value) {
                                if (value.x != null && value.y != null) {
                                    map.geoObjects.add(new ymaps.Placemark([value.x, value.y], {
                                        balloonContent: value.name
                                    }));
                                }
                            });
                        }
                    });
                }
            );
        }
    } else if (map_draw.length) {
        yandex_map_destroy(map_draw);
    }
}

function yandex_map_destroy(map_draw) {
    if (ya_created === true) {
        if (typeof(map) !== 'undefined') {
            map.destroy();
        }

        map_draw.html('').css('height', 'auto');
        ya_created = false;
    }
}